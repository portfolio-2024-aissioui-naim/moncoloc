﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using Coloc.Model;

namespace Coloc.Dao
{
    public class DaoColoc
    {
        /// <summary>
        /// Fonction qui permet d'obtenir la liste de tous les colocataires
        /// </summary>
        /// <returns>Une liste de colocataire</returns>
        public List<Colocataire> GetAll()
        {
            List<Colocataire> listeColoc = new List<Colocataire>();
            MySqlConnection cnx = DaoConnection.GetMySqlConnection();
            cnx.Open();
            MySqlCommand cmd = new MySqlCommand("select id,prenom,nom,mail,telephone from colocataire", cnx);
            MySqlDataReader rdr = cmd.ExecuteReader();
            while (rdr.Read())
            {
                int id = rdr.GetInt32("id");
                string nom = rdr.GetString("nom");
                string prenom = rdr.GetString("prenom");
                string mail = rdr.GetString("mail");
                string tel = rdr.GetString("telephone");
                listeColoc.Add(new Colocataire(id, nom, prenom, mail, tel));
            }
            cnx.Close();
            return listeColoc;
        }

        /// <summary>
        /// Fonction qui permet de récupérer le prénom du colocataire 
        /// </summary>
        /// <param name="idColoc">Id du colocataire</param>
        /// <returns>Le nom du colocataire de type string</returns>
        public string GetColoc(int idColoc)
        {
            MySqlConnection cnx = DaoConnection.GetMySqlConnection();
            cnx.Open();
            MySqlCommand cmd = new MySqlCommand("select Prenom from colocataire where id=@id", cnx);
            cmd.Parameters.Add(new MySqlParameter("@id",MySqlDbType.Int32));
            cmd.Parameters["@id"].Value = idColoc;
            string prenom = Convert.ToString(cmd.ExecuteScalar());
            cnx.Close();
            return prenom;
        }

        /// <summary>
        /// Fonction qui permet d'avoir le nombre de colocataire
        /// </summary>
        /// <returns>Le nombre de colocataire (int)</returns>
        public int GetCout()
        {
            MySqlConnection cnx = DaoConnection.GetMySqlConnection();
            cnx.Open();
            MySqlCommand cmd = new MySqlCommand("select count(*) from colocataire", cnx);
            int count = Convert.ToInt32(cmd.ExecuteScalar());
            cnx.Close();
            return count;
        }

        /// <summary>
        /// Méthode qui permet d'ajouter un colocataire dans la base de donnée
        /// </summary>
        /// <param name="unColoc">Objet de type Colocataire qui correspond à un colocataire</param>
        public void Insert(Colocataire unColoc)
        {
            MySqlConnection cnx = DaoConnection.GetMySqlConnection();
            cnx.Open();
            MySqlCommand cmd = new MySqlCommand("insert into colocataire(prenom,nom, mail, telephone) values(@prenom,@nom,@mail,@tel)", cnx);
            cmd.Parameters.Add(new MySqlParameter("@prenom", MySqlDbType.VarChar));
            cmd.Parameters.Add(new MySqlParameter("@nom", MySqlDbType.VarChar));
            cmd.Parameters.Add(new MySqlParameter("@mail", MySqlDbType.VarChar));
            cmd.Parameters.Add(new MySqlParameter("@tel", MySqlDbType.VarChar));

            cmd.Parameters["@prenom"].Value = unColoc.Prenom;
            cmd.Parameters["@nom"].Value =unColoc.Nom;
            cmd.Parameters["@mail"].Value =unColoc.Mail;
            cmd.Parameters["@tel"].Value =unColoc.Telephone;
            cmd.ExecuteNonQuery();
            cnx.Close();
        }

        /// <summary>
        /// Méthode qui permet de supprimer un colocataire dans la base de donnée
        /// </summary>
        /// <param name="unColoc">Objet de type Colocataire qui correspond à un colocataire</param>
        public void Delete(Colocataire unColoc)
        {
            MySqlConnection cnx = DaoConnection.GetMySqlConnection();
            cnx.Open();
            MySqlCommand cmd = new MySqlCommand("delete from colocataire where id = @id", cnx);
            cmd.Parameters.Add(new MySqlParameter("@id", MySqlDbType.Int32));
            cmd.Parameters["@id"].Value = unColoc.IdColoc;
            cmd.ExecuteNonQuery();
            cnx.Close();
        }

        /// <summary>
        /// Méthode qui permet de modifier un colocataire dans la base de donnée
        /// </summary>
        /// <param name="unColoc">Objet de type Colocataire qui correspond à un colocataire</param>
        public void Update(Colocataire unColoc)
        {
            MySqlConnection cnx = DaoConnection.GetMySqlConnection();
            cnx.Open();
            MySqlCommand cmd = new MySqlCommand("update colocataire set prenom = @prenom, nom = @nom, mail = @mail, telephone = @tel where id = @id",cnx) ;
            cmd.Parameters.Add(new MySqlParameter("@id", MySqlDbType.Int32));
            cmd.Parameters["@id"].Value = unColoc.IdColoc;
            cmd.Parameters.Add(new MySqlParameter("@prenom", MySqlDbType.VarChar));
            cmd.Parameters["@prenom"].Value = unColoc.Prenom;
            cmd.Parameters.Add(new MySqlParameter("@nom", MySqlDbType.VarChar));
            cmd.Parameters["@nom"].Value = unColoc.Nom;
            cmd.Parameters.Add(new MySqlParameter("@mail", MySqlDbType.VarChar));
            cmd.Parameters["@mail"].Value = unColoc.Mail;
            cmd.Parameters.Add(new MySqlParameter("@tel", MySqlDbType.VarChar));
            cmd.Parameters["@tel"].Value = unColoc.Telephone;
            cmd.ExecuteNonQuery();
            cnx.Close();
        }
    }
}
