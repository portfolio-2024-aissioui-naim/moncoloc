﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coloc.Model
{
    public class Colocataire
    {
        private int id;
        private string nom;
        private string prenom;
        private string mail;
        private string telephone;
        private List<Depense> lesDepenses;

        /// <summary>
        /// Constructeur de la classe Colocataire qui permet d'instancier un colocataire
        /// </summary>
        /// <param name="id">Id du colocataire</param>
        /// <param name="nom">Nom du colocataire</param>
        /// <param name="prenom">Prénom du colocataire</param>
        /// <param name="mail">Mail du colocataire</param>
        /// <param name="telephone">Téléphone du colocataire</param>

        public Colocataire(int id, string nom, string prenom, string mail, string telephone)
        {
            this.id = id;
            this.nom = nom;
            this.prenom = prenom;
            this.mail = mail;
            this.telephone = telephone;
            this.lesDepenses = new List<Depense>();
        }

        public Colocataire()
        {

        }

        /// <summary>
        /// Constructeur de la classe Colocataire qui permet d'instancier un colocataire.
        /// </summary>
        /// <param name="nom">Nom du colocataire</param>
        /// <param name="prenom">Prénom du colocataire</param>
        /// <param name="mail">Mail du colocataire</param>
        /// <param name="telephone">Téléphone du colocataire</param>
        public Colocataire(string nom, string prenom, string mail, string telephone)
        {
            this.nom = nom;
            this.prenom = prenom;
            this.mail = mail;
            this.telephone = telephone;
            this.lesDepenses = new List<Depense>();
        }

        /// <summary>
        /// Propriété (set et get) qui permet d'obtenir et de modifier l'id du colocataire
        /// </summary>
        public int IdColoc
        {
            get { return this.id; }
            set { this.id = value; }
        }

        /// <summary>
        /// Propriété (set et get) qui permet d'obtenir et de modifier le nom du colocataire
        /// </summary>
        public string Nom
        {
            get { return this.nom; }
            set { this.nom = value; }
        }

        /// <summary>
        /// Propriété (set et get) qui permet d'obtenir et de modifier le prenom du colocataire
        /// </summary>
        public string Prenom
        {
            get { return this.prenom; }
            set { this.prenom = value; }
        }

        /// <summary>
        /// Propriété (set et get) qui permet d'obtenir et de modifier le mail du colocataire
        /// </summary>
        public string Mail
        {
            get { return this.mail; }
            set { this.mail = value; }
        }

        /// <summary>
        /// Propriété (set et get) qui permet d'obtenir et de modifier le téléphone du colocataire
        /// </summary>
        public string Telephone
        {
            get { return this.telephone; }
            set { this.telephone = value; }
        }
        
        /// <summary>
        /// Propriété (get) qui permet d'obtenir la longueur de la liste des dépenses d'un colocataire
        /// </summary>
        public int Count
        {
            get { return this.lesDepenses.Count; }
        }

        /// <summary>
        /// Méthode qui permet d'ajouter une dépense dans la liste des dépenses d'un colocataire
        /// </summary>
        /// <param name="uneDepense">Objet de type Dépense qui correspond à une dépense d'un colocataire</param>
        public void Add(Depense uneDepense)
        {
            this.lesDepenses.Add(uneDepense);
        }

        /// <summary>
        /// Méthode qui permet de supprimer une dépense dans la liste des dépenses deu
        /// </summary>
        /// <param name="uneDepense">Objet de type Dépense qui correspond à une dépense d'un colocataire</param>
        public void Remove(Depense uneDepense)
        {
            this.lesDepenses.Remove(uneDepense);
        }

        /// <summary>
        /// Fonction qui permet d'obtenir la liste des dépenses du colocataire
        /// </summary>
        /// <returns>Une colection de Dépense</returns>
        public List<Depense> GetListeDepense()
        {
            return this.lesDepenses;
        }

        /// <summary>
        /// Méthode qui permet d'affecter dans la donnée membre la liste des dépenses (variable passée en paramètre)
        /// </summary>
        /// <param name="lesDepenses">Une colection de Dépense</param>
        public void SetListDepense(List<Depense> lesDepenses)
        {
            this.lesDepenses = lesDepenses;
        } 

        /// <summary>
        /// Fonction qui renvoie le montant total qu'a payé le colocataire
        /// </summary>
        /// <returns>Le total en décimal</returns>
        public decimal APaye()
        {
            decimal depenses = 0.0m;
            foreach (Depense uneDepense in this.lesDepenses)
            {
                if(uneDepense.Reparti == false)
                {
                    depenses += uneDepense.Montant;
                }  
            }
            return depenses;
        }

        /// <summary>
        /// Fonction qui renvoie le montant total que doit payer le colocataire
        /// </summary>
        /// <param name="montantDu">Montant que le colocataire doit payer (décimal)</param>
        /// <returns>Le total en décimal</returns>
        public decimal Doit(decimal montantDu)
        {
            decimal doit;
            doit = montantDu - this.APaye();
            return doit;
        }

        public override string ToString()
        {
            return string.Format("Colocataire : {0} {1}     Mail : {2}     Téléphone : {3}", this.nom,this.prenom,this.mail,this.telephone);
        }
    }
}
