﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coloc.Model
{
    public class Colocs
    {
        private List<Coloc> lesColocs;

        public Colocs()
        {
            this.lesColocs = new List<Coloc>();
        }

        public int Count
        {
            get { return this.lesColocs.Count; }
        }

        public void Add(Coloc unColoc)
        {
            this.lesColocs.Add(unColoc);
        }

        public void Remove(Coloc unColoc)
        {
            this.lesColocs.Remove(unColoc);
        }

        public List<Coloc> GetListeColoc()
        {
            return this.lesColocs;
        }
    }
}
