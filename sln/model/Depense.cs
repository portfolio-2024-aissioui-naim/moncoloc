﻿using System.Collections.Generic;
using System;

namespace Coloc.Model
{
    public class Depense
    {
        private int id;
        private DateTime dateDepense;
        private string text;
        private string justificatif;
        private decimal montant;
        private bool reparti;
        private int idColoc;
        private List<Colocataire> lesColocs;

        /// <summary>
        /// Constructeur de la classe Depense qui permet d'instancier une Depense
        /// </summary>
        /// <param name="id">Id de la dépense</param>
        /// <param name="date">Date de la dépense</param>
        /// <param name="text">Désignation de la dépense</param>
        /// <param name="justificatif">Justificatif de la dépense</param>
        /// <param name="montant">Montant de la dépense</param>
        /// <param name="reparti">Répartition de la dépense</param>
        /// <param name="idColoc">Id du colocataire de la dépennse</param>
        public Depense(int id, DateTime date, string text, string justificatif, decimal montant, bool reparti ,int idColoc)
        {
            this.id = id;
            this.dateDepense = date;
            this.text = text;
            this.justificatif = justificatif;
            this.montant = montant;
            this.reparti = reparti;
            this.idColoc = idColoc;
        }

        /// <summary>
        /// Constructeur de la classe Depense qui permet d'instancier une Depense
        /// </summary>
        /// <param name="date">Date de la dépense</param>
        /// <param name="text">Désignation de la dépense</param>
        /// <param name="justificatif">Justificatif de la dépense</param>
        /// <param name="montant">Montant de la dépense</param>
        /// <param name="reparti">Répartition de la dépense</param>
        /// <param name="idColoc">Id du colocataire de la dépennse</param>
        public Depense(DateTime date, string text, string justificatif, decimal montant, bool reparti, int idColoc)
        {
            this.dateDepense = date;
            this.text = text;
            this.justificatif = justificatif;
            this.montant = montant;
            this.reparti = reparti;
            this.idColoc = idColoc;
        }

        /// <summary>
        /// Propriété (get) qui permet d'obtenir l'id du colocataire de la dépense
        /// </summary>
        public int IdColoc
        {
            get { return this.idColoc; }
        }

        /// <summary>
        /// Propriété (get) qui permet d'obtenir l'id de la dépense
        /// </summary>
        public int IdDepense
        {
            get { return this.id; }
        }

        /// <summary>
        /// Propriété (get set) qui permet d'obtenir et de modifier la date de la dépense
        /// </summary>
        public DateTime DateDepense
        {
            get { return this.dateDepense; }
            set { this.dateDepense = value; }
        }

        /// <summary>
        /// Propriété (get set) qui permet d'obtenir et de modifier la désignation de la dépense
        /// </summary>
        public string Text
        {
            get { return this.text; }
            set { this.text = value; }
        }

        /// <summary>
        /// Propriété (get set) qui permet d'obtenir et de modifier le justificatif de la dépense
        /// </summary>
        public string Justificatif
        {
            get { return this.justificatif; }
            set { this.justificatif = value; }
        }

        /// <summary>
        /// Propriété (get set) qui permet d'obtenir et de modifier le montant de la dépense
        /// </summary>
        public decimal Montant
        {
            get { return this.montant; }
            set { this.montant = value; }
        }

        /// <summary>
        /// Propriété (get set) qui permet d'obtenir et de modifier la répartition de la dépense
        /// </summary>
        public bool Reparti
        {
            get { return this.reparti; }
            set { this.reparti = value; }
        }

        /// <summary>
        /// Méthode qui permet d'affecter dans la donnée membre la liste des colocataires (variable passée en paramètre)
        /// </summary>
        /// <param name="lesColocs">Une collection de Colocataire</param>
        public void SetListColoc(List<Colocataire> lesColocs)
        {
            this.lesColocs = lesColocs;
        }

        /// <summary>
        /// Fonction qui permet de trouver un colocataire en comparant l'id du colocataire qui est associé à la dépense avec la liste des colocataires
        /// </summary>
        /// <returns>Le nom du colocataire (string)</returns>
        private string GetPrenomColoc()
        {
            string prenom = "";
            foreach(Colocataire unColoc in this.lesColocs)
            {
                if(unColoc.IdColoc == idColoc)
                {
                    prenom = unColoc.Prenom;
                }
            }
            return prenom;
        }

        public override string ToString()
        {
            return string.Format("Dépense de {0} -> Désignation : {1}     Date : {2}     Montant : {3} €", this.GetPrenomColoc(), this.text, this.dateDepense, this.montant);
        }
    }
}
