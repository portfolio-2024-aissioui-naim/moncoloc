﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coloc.Model
{
    public class DepensesNonReparties
    {
        //private List<Depense> lesDepenses;

        /// <summary>
        /// Constructeur de la classe DepensesNonReparties qui permet d'instancier une liste de dépenses non reparties
        /// </summary>
        public DepensesNonReparties()
        {
            //this.lesDepenses = new List<Depense>();
        }

        /// <summary>
        /// Fonction qui permet de calculer ce que doit payer chaque colocataire
        /// </summary>
        /// <param name="countColocataire">Nombre de colocataire</param>
        /// <returns>Le montant que chacun doit payer en décimal</returns>
        public decimal MontantDu(int countColocataire, List<Depense> lesDepenses)
        {
            decimal montantDu;
            montantDu = this.TotalDesDepenses(lesDepenses) / countColocataire;
            return montantDu;
        }

        /// <summary>
        /// Fonction qui permet de calculer le montant total des dépenses non reparties de tous les colocataires
        /// </summary>
        /// <returns>Le montant Le montant total des dépenses non reparties en décimal</returns>
        public decimal TotalDesDepenses(List <Depense> lesDepenses)
        {
            decimal totalDepenseNonReparti = 0.0m;
            foreach (Depense uneDepense in lesDepenses)
            {  
                totalDepenseNonReparti += uneDepense.Montant;
            }
            return totalDepenseNonReparti;
        }
    }
}
