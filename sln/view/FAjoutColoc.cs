﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Coloc.Dao;
using Coloc.Model;

namespace Coloc.View
{
    public partial class FAjoutColoc : Form
    {
        private FColocataires f;
        public FAjoutColoc(FColocataires f)
        {
            InitializeComponent();
            this.f = f;
            btnAjouterColoc.Click += BtnAjouterColoc_Click;
            btnRetourColoc.Click += BtnRetourColoc_Click;
            this.FormClosing += FAjoutColoc_FormClosing;
        }

        private void FAjoutColoc_FormClosing(object sender, FormClosingEventArgs e)
        {
            FColocataires f = new FColocataires();
            f.Show();
        }

        private void BtnRetourColoc_Click(object sender, EventArgs e)
        {
            this.Hide();
            FColocataires f = new FColocataires();
            f.Show();
        }

        private void BtnAjouterColoc_Click(object sender, EventArgs e)
        {
            if(tbAjoutNom.Text != "" && tbAjoutMail.Text != "" && tbAjoutPrenom.Text != "" && tbAjoutTel.Text != "")
            {    
                Colocataire unColoc = new Colocataire(tbAjoutNom.Text, tbAjoutPrenom.Text, tbAjoutMail.Text, tbAjoutTel.Text);
                DaoColoc daoColoc = new DaoColoc();
                daoColoc.Insert(unColoc);
                tbAjoutPrenom.Text = "";
                tbAjoutNom.Text = "";
                tbAjoutTel.Text = "";
                tbAjoutMail.Text = "";
                f.Clear();
                f.LoadListe(daoColoc.GetAll());
                this.Hide();
                f.Show();
            }
            else
            {
                MessageBox.Show("Ne pas laisser des champs vides !");
            }
        }
    }
}
