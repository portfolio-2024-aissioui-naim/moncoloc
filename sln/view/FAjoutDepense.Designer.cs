﻿
namespace Coloc.View
{
    partial class FAjoutDepense
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FAjoutDepense));
            this.labelAjouterDepense = new System.Windows.Forms.Label();
            this.dtpAjoutDate = new System.Windows.Forms.DateTimePicker();
            this.labelDateDepense = new System.Windows.Forms.Label();
            this.labelMontant = new System.Windows.Forms.Label();
            this.labelDesignation = new System.Windows.Forms.Label();
            this.labelJustificatif = new System.Windows.Forms.Label();
            this.tbAjoutDesignation = new System.Windows.Forms.TextBox();
            this.tbAjoutJustificatif = new System.Windows.Forms.TextBox();
            this.tbAjoutMontant = new System.Windows.Forms.TextBox();
            this.btnAjouterDepense = new System.Windows.Forms.Button();
            this.cbColocataire = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnRetourDepense = new System.Windows.Forms.Button();
            this.od_ouvrir = new System.Windows.Forms.OpenFileDialog();
            this.btnOpen = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelAjouterDepense
            // 
            this.labelAjouterDepense.AutoSize = true;
            this.labelAjouterDepense.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.labelAjouterDepense.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAjouterDepense.ForeColor = System.Drawing.Color.Snow;
            this.labelAjouterDepense.Location = new System.Drawing.Point(279, 49);
            this.labelAjouterDepense.Name = "labelAjouterDepense";
            this.labelAjouterDepense.Size = new System.Drawing.Size(255, 29);
            this.labelAjouterDepense.TabIndex = 5;
            this.labelAjouterDepense.Text = "Ajouter une dépense";
            // 
            // dtpAjoutDate
            // 
            this.dtpAjoutDate.Location = new System.Drawing.Point(297, 136);
            this.dtpAjoutDate.Name = "dtpAjoutDate";
            this.dtpAjoutDate.Size = new System.Drawing.Size(281, 20);
            this.dtpAjoutDate.TabIndex = 9;
            // 
            // labelDateDepense
            // 
            this.labelDateDepense.AutoSize = true;
            this.labelDateDepense.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.labelDateDepense.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDateDepense.ForeColor = System.Drawing.Color.Snow;
            this.labelDateDepense.Location = new System.Drawing.Point(89, 135);
            this.labelDateDepense.Name = "labelDateDepense";
            this.labelDateDepense.Size = new System.Drawing.Size(133, 16);
            this.labelDateDepense.TabIndex = 10;
            this.labelDateDepense.Text = "Date de la dépense :";
            // 
            // labelMontant
            // 
            this.labelMontant.AutoSize = true;
            this.labelMontant.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.labelMontant.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMontant.ForeColor = System.Drawing.Color.Snow;
            this.labelMontant.Location = new System.Drawing.Point(89, 293);
            this.labelMontant.Name = "labelMontant";
            this.labelMontant.Size = new System.Drawing.Size(61, 16);
            this.labelMontant.TabIndex = 11;
            this.labelMontant.Text = "Montant :";
            // 
            // labelDesignation
            // 
            this.labelDesignation.AutoSize = true;
            this.labelDesignation.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.labelDesignation.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDesignation.ForeColor = System.Drawing.Color.Snow;
            this.labelDesignation.Location = new System.Drawing.Point(89, 183);
            this.labelDesignation.Name = "labelDesignation";
            this.labelDesignation.Size = new System.Drawing.Size(86, 16);
            this.labelDesignation.TabIndex = 12;
            this.labelDesignation.Text = "Désignation :";
            // 
            // labelJustificatif
            // 
            this.labelJustificatif.AutoSize = true;
            this.labelJustificatif.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.labelJustificatif.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelJustificatif.ForeColor = System.Drawing.Color.Snow;
            this.labelJustificatif.Location = new System.Drawing.Point(89, 230);
            this.labelJustificatif.Name = "labelJustificatif";
            this.labelJustificatif.Size = new System.Drawing.Size(71, 16);
            this.labelJustificatif.TabIndex = 13;
            this.labelJustificatif.Text = "Justificatif :";
            // 
            // tbAjoutDesignation
            // 
            this.tbAjoutDesignation.Location = new System.Drawing.Point(297, 180);
            this.tbAjoutDesignation.Name = "tbAjoutDesignation";
            this.tbAjoutDesignation.Size = new System.Drawing.Size(281, 20);
            this.tbAjoutDesignation.TabIndex = 15;
            // 
            // tbAjoutJustificatif
            // 
            this.tbAjoutJustificatif.Location = new System.Drawing.Point(297, 226);
            this.tbAjoutJustificatif.Margin = new System.Windows.Forms.Padding(0);
            this.tbAjoutJustificatif.Name = "tbAjoutJustificatif";
            this.tbAjoutJustificatif.Size = new System.Drawing.Size(281, 20);
            this.tbAjoutJustificatif.TabIndex = 16;
            // 
            // tbAjoutMontant
            // 
            this.tbAjoutMontant.Location = new System.Drawing.Point(297, 290);
            this.tbAjoutMontant.Name = "tbAjoutMontant";
            this.tbAjoutMontant.Size = new System.Drawing.Size(281, 20);
            this.tbAjoutMontant.TabIndex = 17;
            // 
            // btnAjouterDepense
            // 
            this.btnAjouterDepense.BackColor = System.Drawing.Color.Snow;
            this.btnAjouterDepense.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAjouterDepense.FlatAppearance.BorderSize = 0;
            this.btnAjouterDepense.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAjouterDepense.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAjouterDepense.Location = new System.Drawing.Point(350, 342);
            this.btnAjouterDepense.Name = "btnAjouterDepense";
            this.btnAjouterDepense.Size = new System.Drawing.Size(162, 30);
            this.btnAjouterDepense.TabIndex = 21;
            this.btnAjouterDepense.Text = "Ajouter la dépense ✔️";
            this.btnAjouterDepense.UseVisualStyleBackColor = false;
            // 
            // cbColocataire
            // 
            this.cbColocataire.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbColocataire.FormattingEnabled = true;
            this.cbColocataire.Location = new System.Drawing.Point(297, 94);
            this.cbColocataire.Name = "cbColocataire";
            this.cbColocataire.Size = new System.Drawing.Size(281, 21);
            this.cbColocataire.TabIndex = 22;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Snow;
            this.label1.Location = new System.Drawing.Point(86, 94);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(176, 16);
            this.label1.TabIndex = 23;
            this.label1.Text = "Sélectionner un colocataire :";
            // 
            // btnRetourDepense
            // 
            this.btnRetourDepense.BackColor = System.Drawing.Color.Snow;
            this.btnRetourDepense.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRetourDepense.FlatAppearance.BorderSize = 0;
            this.btnRetourDepense.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRetourDepense.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRetourDepense.Location = new System.Drawing.Point(12, 327);
            this.btnRetourDepense.Name = "btnRetourDepense";
            this.btnRetourDepense.Size = new System.Drawing.Size(117, 60);
            this.btnRetourDepense.TabIndex = 25;
            this.btnRetourDepense.Text = "Retourner sur la page précédente ↩️";
            this.btnRetourDepense.UseVisualStyleBackColor = false;
            // 
            // od_ouvrir
            // 
            this.od_ouvrir.FileName = "openFileDialog1";
            // 
            // btnOpen
            // 
            this.btnOpen.BackColor = System.Drawing.Color.Snow;
            this.btnOpen.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnOpen.FlatAppearance.BorderSize = 0;
            this.btnOpen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOpen.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.btnOpen.Location = new System.Drawing.Point(297, 246);
            this.btnOpen.Margin = new System.Windows.Forms.Padding(0);
            this.btnOpen.Name = "btnOpen";
            this.btnOpen.Size = new System.Drawing.Size(281, 23);
            this.btnOpen.TabIndex = 26;
            this.btnOpen.Text = "Sélectionner un fichier 📁";
            this.btnOpen.UseVisualStyleBackColor = false;
            // 
            // FAjoutDepense
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.ClientSize = new System.Drawing.Size(721, 401);
            this.Controls.Add(this.btnOpen);
            this.Controls.Add(this.btnRetourDepense);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbColocataire);
            this.Controls.Add(this.btnAjouterDepense);
            this.Controls.Add(this.tbAjoutMontant);
            this.Controls.Add(this.tbAjoutJustificatif);
            this.Controls.Add(this.tbAjoutDesignation);
            this.Controls.Add(this.labelJustificatif);
            this.Controls.Add(this.labelDesignation);
            this.Controls.Add(this.labelMontant);
            this.Controls.Add(this.labelDateDepense);
            this.Controls.Add(this.dtpAjoutDate);
            this.Controls.Add(this.labelAjouterDepense);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FAjoutDepense";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ajouter une dépense";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelAjouterDepense;
        private System.Windows.Forms.DateTimePicker dtpAjoutDate;
        private System.Windows.Forms.Label labelDateDepense;
        private System.Windows.Forms.Label labelMontant;
        private System.Windows.Forms.Label labelDesignation;
        private System.Windows.Forms.Label labelJustificatif;
        private System.Windows.Forms.TextBox tbAjoutDesignation;
        private System.Windows.Forms.TextBox tbAjoutJustificatif;
        private System.Windows.Forms.TextBox tbAjoutMontant;
        private System.Windows.Forms.Button btnAjouterDepense;
        private System.Windows.Forms.ComboBox cbColocataire;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnRetourDepense;
        private System.Windows.Forms.OpenFileDialog od_ouvrir;
        private System.Windows.Forms.Button btnOpen;
    }
}