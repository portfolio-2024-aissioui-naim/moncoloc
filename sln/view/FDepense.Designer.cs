﻿
namespace Coloc.View
{
    partial class FDepense
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FDepense));
            this.labelConsultationDepense = new System.Windows.Forms.Label();
            this.lbDepense = new System.Windows.Forms.ListBox();
            this.btnRetourDepense = new System.Windows.Forms.Button();
            this.btnSuppDepense = new System.Windows.Forms.Button();
            this.btnModifDepense = new System.Windows.Forms.Button();
            this.btnSaisirDepense = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btnReparti = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelConsultationDepense
            // 
            this.labelConsultationDepense.AutoSize = true;
            this.labelConsultationDepense.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.labelConsultationDepense.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelConsultationDepense.ForeColor = System.Drawing.Color.Snow;
            this.labelConsultationDepense.Location = new System.Drawing.Point(68, 41);
            this.labelConsultationDepense.Name = "labelConsultationDepense";
            this.labelConsultationDepense.Size = new System.Drawing.Size(477, 29);
            this.labelConsultationDepense.TabIndex = 5;
            this.labelConsultationDepense.Text = "Consultation des dépenses Non Reparti";
            // 
            // lbDepense
            // 
            this.lbDepense.FormattingEnabled = true;
            this.lbDepense.Location = new System.Drawing.Point(177, 96);
            this.lbDepense.Name = "lbDepense";
            this.lbDepense.Size = new System.Drawing.Size(611, 342);
            this.lbDepense.TabIndex = 6;
            // 
            // btnRetourDepense
            // 
            this.btnRetourDepense.BackColor = System.Drawing.Color.Snow;
            this.btnRetourDepense.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRetourDepense.FlatAppearance.BorderSize = 0;
            this.btnRetourDepense.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRetourDepense.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRetourDepense.Location = new System.Drawing.Point(26, 378);
            this.btnRetourDepense.Name = "btnRetourDepense";
            this.btnRetourDepense.Size = new System.Drawing.Size(117, 60);
            this.btnRetourDepense.TabIndex = 7;
            this.btnRetourDepense.Text = "Retourner sur la page précédente ↩️";
            this.btnRetourDepense.UseVisualStyleBackColor = false;
            // 
            // btnSuppDepense
            // 
            this.btnSuppDepense.BackColor = System.Drawing.Color.Snow;
            this.btnSuppDepense.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSuppDepense.FlatAppearance.BorderColor = System.Drawing.Color.Red;
            this.btnSuppDepense.FlatAppearance.BorderSize = 0;
            this.btnSuppDepense.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSuppDepense.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSuppDepense.Location = new System.Drawing.Point(12, 291);
            this.btnSuppDepense.Name = "btnSuppDepense";
            this.btnSuppDepense.Size = new System.Drawing.Size(151, 46);
            this.btnSuppDepense.TabIndex = 12;
            this.btnSuppDepense.Text = "Supprimer une dépense 🗑️  ";
            this.btnSuppDepense.UseVisualStyleBackColor = false;
            // 
            // btnModifDepense
            // 
            this.btnModifDepense.BackColor = System.Drawing.Color.Snow;
            this.btnModifDepense.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnModifDepense.FlatAppearance.BorderSize = 0;
            this.btnModifDepense.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnModifDepense.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModifDepense.Location = new System.Drawing.Point(12, 201);
            this.btnModifDepense.Name = "btnModifDepense";
            this.btnModifDepense.Size = new System.Drawing.Size(151, 46);
            this.btnModifDepense.TabIndex = 11;
            this.btnModifDepense.Text = "Modifier une dépense 🖊️";
            this.btnModifDepense.UseVisualStyleBackColor = false;
            // 
            // btnSaisirDepense
            // 
            this.btnSaisirDepense.BackColor = System.Drawing.Color.Snow;
            this.btnSaisirDepense.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSaisirDepense.FlatAppearance.BorderSize = 0;
            this.btnSaisirDepense.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSaisirDepense.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSaisirDepense.Location = new System.Drawing.Point(12, 112);
            this.btnSaisirDepense.Name = "btnSaisirDepense";
            this.btnSaisirDepense.Size = new System.Drawing.Size(151, 46);
            this.btnSaisirDepense.TabIndex = 10;
            this.btnSaisirDepense.Text = "Saisir une dépense ➕";
            this.btnSaisirDepense.UseVisualStyleBackColor = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Snow;
            this.label1.Location = new System.Drawing.Point(595, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 18);
            this.label1.TabIndex = 13;
            this.label1.Text = "Dépense :";
            // 
            // btnReparti
            // 
            this.btnReparti.BackColor = System.Drawing.Color.Snow;
            this.btnReparti.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnReparti.FlatAppearance.BorderSize = 0;
            this.btnReparti.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReparti.Location = new System.Drawing.Point(676, 45);
            this.btnReparti.Name = "btnReparti";
            this.btnReparti.Size = new System.Drawing.Size(103, 29);
            this.btnReparti.TabIndex = 14;
            this.btnReparti.Text = "Reparti";
            this.btnReparti.UseVisualStyleBackColor = false;
            // 
            // FDepense
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnReparti);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnSuppDepense);
            this.Controls.Add(this.btnModifDepense);
            this.Controls.Add(this.btnSaisirDepense);
            this.Controls.Add(this.btnRetourDepense);
            this.Controls.Add(this.lbDepense);
            this.Controls.Add(this.labelConsultationDepense);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FDepense";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Les dépenses";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelConsultationDepense;
        private System.Windows.Forms.ListBox lbDepense;
        private System.Windows.Forms.Button btnRetourDepense;
        private System.Windows.Forms.Button btnSuppDepense;
        private System.Windows.Forms.Button btnModifDepense;
        private System.Windows.Forms.Button btnSaisirDepense;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnReparti;
    }
}