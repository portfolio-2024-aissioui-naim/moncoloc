﻿namespace Coloc.View {
    partial class FMenu {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FMenu));
            this.labelMenu = new System.Windows.Forms.Label();
            this.btnGererRepartitions = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.LabelGroupPrenom = new System.Windows.Forms.Label();
            this.btnGererColocataires = new System.Windows.Forms.Button();
            this.btnGererDepenses = new System.Windows.Forms.Button();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // labelMenu
            // 
            this.labelMenu.AutoSize = true;
            this.labelMenu.BackColor = System.Drawing.Color.Transparent;
            this.labelMenu.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.labelMenu.Font = new System.Drawing.Font("Trebuchet MS", 15.75F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic) 
                | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMenu.ForeColor = System.Drawing.Color.Snow;
            this.labelMenu.Location = new System.Drawing.Point(220, 184);
            this.labelMenu.Name = "labelMenu";
            this.labelMenu.Size = new System.Drawing.Size(365, 27);
            this.labelMenu.TabIndex = 0;
            this.labelMenu.Text = "Menu de l\'application \"Colocataire\"";
            // 
            // btnGererRepartitions
            // 
            this.btnGererRepartitions.BackColor = System.Drawing.Color.Snow;
            this.btnGererRepartitions.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnGererRepartitions.FlatAppearance.BorderSize = 0;
            this.btnGererRepartitions.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGererRepartitions.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGererRepartitions.Location = new System.Drawing.Point(499, 260);
            this.btnGererRepartitions.Name = "btnGererRepartitions";
            this.btnGererRepartitions.Size = new System.Drawing.Size(86, 45);
            this.btnGererRepartitions.TabIndex = 4;
            this.btnGererRepartitions.Text = "Gérer les répartitions";
            this.btnGererRepartitions.UseVisualStyleBackColor = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Trebuchet MS", 6.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Snow;
            this.label2.Location = new System.Drawing.Point(12, 426);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(126, 15);
            this.label2.TabIndex = 6;
            this.label2.Text = "BTS SIO 2 (Florian, Thibault)";
            // 
            // LabelGroupPrenom
            // 
            this.LabelGroupPrenom.AutoSize = true;
            this.LabelGroupPrenom.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.LabelGroupPrenom.Font = new System.Drawing.Font("Trebuchet MS", 6.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelGroupPrenom.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.LabelGroupPrenom.Location = new System.Drawing.Point(12, 430);
            this.LabelGroupPrenom.Name = "LabelGroupPrenom";
            this.LabelGroupPrenom.Size = new System.Drawing.Size(126, 15);
            this.LabelGroupPrenom.TabIndex = 6;
            this.LabelGroupPrenom.Text = "BTS SIO 2 (Florian, Thibault)";
            // 
            // btnGererColocataires
            // 
            this.btnGererColocataires.BackColor = System.Drawing.Color.Snow;
            this.btnGererColocataires.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnGererColocataires.FlatAppearance.BorderSize = 0;
            this.btnGererColocataires.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGererColocataires.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGererColocataires.Location = new System.Drawing.Point(225, 260);
            this.btnGererColocataires.Name = "btnGererColocataires";
            this.btnGererColocataires.Size = new System.Drawing.Size(86, 45);
            this.btnGererColocataires.TabIndex = 3;
            this.btnGererColocataires.Text = "Gérer les colocataires";
            this.btnGererColocataires.UseVisualStyleBackColor = false;
            // 
            // btnGererDepenses
            // 
            this.btnGererDepenses.BackColor = System.Drawing.Color.Snow;
            this.btnGererDepenses.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnGererDepenses.FlatAppearance.BorderSize = 0;
            this.btnGererDepenses.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGererDepenses.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGererDepenses.Location = new System.Drawing.Point(363, 260);
            this.btnGererDepenses.Name = "btnGererDepenses";
            this.btnGererDepenses.Size = new System.Drawing.Size(86, 45);
            this.btnGererDepenses.TabIndex = 2;
            this.btnGererDepenses.Text = "Gérer ses dépenses";
            this.btnGererDepenses.UseVisualStyleBackColor = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.Color.Snow;
            this.pictureBox5.Image = global::Coloc.View.Properties.Resources.CrousParis_removebg_preview;
            this.pictureBox5.Location = new System.Drawing.Point(307, 21);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(400, 66);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox5.TabIndex = 13;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.Black;
            this.pictureBox4.Location = new System.Drawing.Point(-24, 103);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(831, 3);
            this.pictureBox4.TabIndex = 11;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Snow;
            this.pictureBox3.Location = new System.Drawing.Point(237, -6);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(569, 111);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 10;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Snow;
            this.pictureBox1.Image = global::Coloc.View.Properties.Resources.crousLogo_removebg_preview;
            this.pictureBox1.Location = new System.Drawing.Point(-1, -6);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(244, 111);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.pictureBox2.Cursor = System.Windows.Forms.Cursors.AppStarting;
            this.pictureBox2.Location = new System.Drawing.Point(796, 446);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(10, 10);
            this.pictureBox2.TabIndex = 8;
            this.pictureBox2.TabStop = false;
            // 
            // FMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.ClientSize = new System.Drawing.Size(804, 454);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnGererRepartitions);
            this.Controls.Add(this.LabelGroupPrenom);
            this.Controls.Add(this.labelMenu);
            this.Controls.Add(this.btnGererDepenses);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.btnGererColocataires);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Menu";
            this.TransparencyKey = System.Drawing.Color.Transparent;
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelMenu;
        private System.Windows.Forms.Button btnGererRepartitions;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnGererColocataires;
        private System.Windows.Forms.Button btnGererDepenses;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label LabelGroupPrenom;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
    }
}

