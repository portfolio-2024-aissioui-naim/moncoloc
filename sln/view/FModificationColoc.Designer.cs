﻿
namespace Coloc.View
{
    partial class FModificationColoc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FModificationColoc));
            this.btnModificationColoc = new System.Windows.Forms.Button();
            this.tbModifMail = new System.Windows.Forms.TextBox();
            this.tbModifNom = new System.Windows.Forms.TextBox();
            this.tbModifPrenom = new System.Windows.Forms.TextBox();
            this.labelModifNom = new System.Windows.Forms.Label();
            this.labelModifPrenom = new System.Windows.Forms.Label();
            this.labelModifMail = new System.Windows.Forms.Label();
            this.labelModifColoc = new System.Windows.Forms.Label();
            this.tbModifTel = new System.Windows.Forms.TextBox();
            this.labelModifTel = new System.Windows.Forms.Label();
            this.btnRetourColoc = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnModificationColoc
            // 
            this.btnModificationColoc.BackColor = System.Drawing.Color.Snow;
            this.btnModificationColoc.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnModificationColoc.FlatAppearance.BorderSize = 0;
            this.btnModificationColoc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnModificationColoc.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModificationColoc.Location = new System.Drawing.Point(340, 348);
            this.btnModificationColoc.Name = "btnModificationColoc";
            this.btnModificationColoc.Size = new System.Drawing.Size(178, 30);
            this.btnModificationColoc.TabIndex = 49;
            this.btnModificationColoc.Text = "Modifer le colocataire ✔️";
            this.btnModificationColoc.UseVisualStyleBackColor = false;
            // 
            // tbModifMail
            // 
            this.tbModifMail.Location = new System.Drawing.Point(302, 240);
            this.tbModifMail.Name = "tbModifMail";
            this.tbModifMail.Size = new System.Drawing.Size(281, 20);
            this.tbModifMail.TabIndex = 46;
            // 
            // tbModifNom
            // 
            this.tbModifNom.Location = new System.Drawing.Point(302, 185);
            this.tbModifNom.Name = "tbModifNom";
            this.tbModifNom.Size = new System.Drawing.Size(281, 20);
            this.tbModifNom.TabIndex = 45;
            // 
            // tbModifPrenom
            // 
            this.tbModifPrenom.Location = new System.Drawing.Point(302, 133);
            this.tbModifPrenom.Name = "tbModifPrenom";
            this.tbModifPrenom.Size = new System.Drawing.Size(281, 20);
            this.tbModifPrenom.TabIndex = 44;
            // 
            // labelModifNom
            // 
            this.labelModifNom.AutoSize = true;
            this.labelModifNom.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.labelModifNom.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelModifNom.ForeColor = System.Drawing.Color.Snow;
            this.labelModifNom.Location = new System.Drawing.Point(198, 189);
            this.labelModifNom.Name = "labelModifNom";
            this.labelModifNom.Size = new System.Drawing.Size(43, 16);
            this.labelModifNom.TabIndex = 42;
            this.labelModifNom.Text = "Nom :";
            // 
            // labelModifPrenom
            // 
            this.labelModifPrenom.AutoSize = true;
            this.labelModifPrenom.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.labelModifPrenom.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelModifPrenom.ForeColor = System.Drawing.Color.Snow;
            this.labelModifPrenom.Location = new System.Drawing.Point(198, 137);
            this.labelModifPrenom.Name = "labelModifPrenom";
            this.labelModifPrenom.Size = new System.Drawing.Size(61, 16);
            this.labelModifPrenom.TabIndex = 41;
            this.labelModifPrenom.Text = "Prénom :";
            // 
            // labelModifMail
            // 
            this.labelModifMail.AutoSize = true;
            this.labelModifMail.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.labelModifMail.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelModifMail.ForeColor = System.Drawing.Color.Snow;
            this.labelModifMail.Location = new System.Drawing.Point(198, 243);
            this.labelModifMail.Name = "labelModifMail";
            this.labelModifMail.Size = new System.Drawing.Size(39, 16);
            this.labelModifMail.TabIndex = 40;
            this.labelModifMail.Text = "Mail :";
            // 
            // labelModifColoc
            // 
            this.labelModifColoc.AutoSize = true;
            this.labelModifColoc.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.labelModifColoc.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelModifColoc.ForeColor = System.Drawing.Color.Snow;
            this.labelModifColoc.Location = new System.Drawing.Point(297, 46);
            this.labelModifColoc.Name = "labelModifColoc";
            this.labelModifColoc.Size = new System.Drawing.Size(280, 29);
            this.labelModifColoc.TabIndex = 34;
            this.labelModifColoc.Text = "Modifier un colocataire";
            // 
            // tbModifTel
            // 
            this.tbModifTel.Location = new System.Drawing.Point(302, 293);
            this.tbModifTel.Name = "tbModifTel";
            this.tbModifTel.Size = new System.Drawing.Size(281, 20);
            this.tbModifTel.TabIndex = 51;
            // 
            // labelModifTel
            // 
            this.labelModifTel.AutoSize = true;
            this.labelModifTel.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.labelModifTel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelModifTel.ForeColor = System.Drawing.Color.Snow;
            this.labelModifTel.Location = new System.Drawing.Point(198, 297);
            this.labelModifTel.Name = "labelModifTel";
            this.labelModifTel.Size = new System.Drawing.Size(80, 16);
            this.labelModifTel.TabIndex = 50;
            this.labelModifTel.Text = "Téléphone :";
            // 
            // btnRetourColoc
            // 
            this.btnRetourColoc.BackColor = System.Drawing.Color.Snow;
            this.btnRetourColoc.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRetourColoc.FlatAppearance.BorderSize = 0;
            this.btnRetourColoc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRetourColoc.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRetourColoc.Location = new System.Drawing.Point(12, 378);
            this.btnRetourColoc.Name = "btnRetourColoc";
            this.btnRetourColoc.Size = new System.Drawing.Size(117, 60);
            this.btnRetourColoc.TabIndex = 52;
            this.btnRetourColoc.Text = "Retourner sur la page précédente ↩️";
            this.btnRetourColoc.UseVisualStyleBackColor = false;
            // 
            // FModificationColoc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.ClientSize = new System.Drawing.Size(889, 450);
            this.Controls.Add(this.btnRetourColoc);
            this.Controls.Add(this.tbModifTel);
            this.Controls.Add(this.labelModifTel);
            this.Controls.Add(this.btnModificationColoc);
            this.Controls.Add(this.tbModifMail);
            this.Controls.Add(this.tbModifNom);
            this.Controls.Add(this.tbModifPrenom);
            this.Controls.Add(this.labelModifNom);
            this.Controls.Add(this.labelModifPrenom);
            this.Controls.Add(this.labelModifMail);
            this.Controls.Add(this.labelModifColoc);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FModificationColoc";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Modifier un colocataire";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnModificationColoc;
        private System.Windows.Forms.TextBox tbModifMail;
        private System.Windows.Forms.TextBox tbModifNom;
        private System.Windows.Forms.TextBox tbModifPrenom;
        private System.Windows.Forms.Label labelModifNom;
        private System.Windows.Forms.Label labelModifPrenom;
        private System.Windows.Forms.Label labelModifMail;
        private System.Windows.Forms.Label labelModifColoc;
        private System.Windows.Forms.TextBox tbModifTel;
        private System.Windows.Forms.Label labelModifTel;
        private System.Windows.Forms.Button btnRetourColoc;
    }
}