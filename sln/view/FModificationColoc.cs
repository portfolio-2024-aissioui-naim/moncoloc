﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Coloc.Dao;
using Coloc.Model;

namespace Coloc.View
{
    public partial class FModificationColoc : Form
    {
        private int idColoc;
        private string nom;
        private string prenom;
        private string mail;
        private string tel;
        private FColocataires f;
        public FModificationColoc(FColocataires f, int idColoc,string nom,string prenom,string mail,string tel)
        {
            InitializeComponent();
            this.idColoc = idColoc;
            this.nom = nom;
            this.prenom = prenom;
            this.mail = mail;
            this.tel = tel;
            tbModifNom.Text = nom;
            tbModifPrenom.Text = prenom;
            tbModifMail.Text = mail;
            tbModifTel.Text = tel;
            this.f = f;
            btnModificationColoc.Click += BtnModificationColoc_Click;
            btnRetourColoc.Click += BtnRetourColoc_Click;
            this.FormClosing += FModificationColoc_FormClosing;
        }

        private void FModificationColoc_FormClosing(object sender, FormClosingEventArgs e)
        {
            FColocataires f = new FColocataires();
            f.Show();
        }

        private void BtnRetourColoc_Click(object sender, EventArgs e)
        {
            this.Hide();
            FColocataires f = new FColocataires();
            f.Show();
        }

        private void BtnModificationColoc_Click(object sender, EventArgs e)
        {
            if (tbModifNom.Text != "" && tbModifPrenom.Text != "" && tbModifMail.Text != "" && tbModifTel.Text != "")
            {
                DaoColoc daoColoc = new DaoColoc();
                daoColoc.Update(new Colocataire(this.idColoc, tbModifNom.Text, tbModifPrenom.Text, tbModifMail.Text, tbModifTel.Text));
                tbModifNom.Text = "";
                tbModifPrenom.Text = "";
                tbModifMail.Text = "";
                tbModifTel.Text = "";
                f.Clear();
                f.LoadListe(daoColoc.GetAll());
                this.Hide();
                f.Show();
            }
            else
            {
                MessageBox.Show("Ne pas laisser des champs vides !");
            }
                
        }
    }
}
