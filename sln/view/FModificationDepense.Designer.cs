﻿
namespace Coloc.View
{
    partial class FModificationDepense
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FModificationDepense));
            this.labelModifDepense = new System.Windows.Forms.Label();
            this.btnModificationDepense = new System.Windows.Forms.Button();
            this.rbFalseModif = new System.Windows.Forms.RadioButton();
            this.rbTrueModif = new System.Windows.Forms.RadioButton();
            this.tbModifMontant = new System.Windows.Forms.TextBox();
            this.tbModifJustificatif = new System.Windows.Forms.TextBox();
            this.tbModifDesignation = new System.Windows.Forms.TextBox();
            this.labelModifReparti = new System.Windows.Forms.Label();
            this.labelModifJustificatif = new System.Windows.Forms.Label();
            this.labelModifDesignation = new System.Windows.Forms.Label();
            this.labelModifMontant = new System.Windows.Forms.Label();
            this.labelModifDateDepense = new System.Windows.Forms.Label();
            this.dtpModifDate = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.cbColocataire = new System.Windows.Forms.ComboBox();
            this.btnRetourDepense = new System.Windows.Forms.Button();
            this.od_ouvrir = new System.Windows.Forms.OpenFileDialog();
            this.btnOpen = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelModifDepense
            // 
            this.labelModifDepense.AutoSize = true;
            this.labelModifDepense.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.labelModifDepense.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelModifDepense.ForeColor = System.Drawing.Color.Snow;
            this.labelModifDepense.Location = new System.Drawing.Point(303, 48);
            this.labelModifDepense.Name = "labelModifDepense";
            this.labelModifDepense.Size = new System.Drawing.Size(268, 29);
            this.labelModifDepense.TabIndex = 6;
            this.labelModifDepense.Text = "Modifier une dépense";
            // 
            // btnModificationDepense
            // 
            this.btnModificationDepense.BackColor = System.Drawing.Color.Snow;
            this.btnModificationDepense.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnModificationDepense.FlatAppearance.BorderSize = 0;
            this.btnModificationDepense.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnModificationDepense.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModificationDepense.Location = new System.Drawing.Point(406, 378);
            this.btnModificationDepense.Name = "btnModificationDepense";
            this.btnModificationDepense.Size = new System.Drawing.Size(165, 30);
            this.btnModificationDepense.TabIndex = 33;
            this.btnModificationDepense.Text = "Modifer la dépense ✔️";
            this.btnModificationDepense.UseVisualStyleBackColor = false;
            // 
            // rbFalseModif
            // 
            this.rbFalseModif.AutoSize = true;
            this.rbFalseModif.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rbFalseModif.Enabled = false;
            this.rbFalseModif.FlatAppearance.BorderSize = 0;
            this.rbFalseModif.ForeColor = System.Drawing.Color.Snow;
            this.rbFalseModif.Location = new System.Drawing.Point(537, 344);
            this.rbFalseModif.Name = "rbFalseModif";
            this.rbFalseModif.Size = new System.Drawing.Size(45, 17);
            this.rbFalseModif.TabIndex = 32;
            this.rbFalseModif.Text = "Non";
            this.rbFalseModif.UseVisualStyleBackColor = true;
            // 
            // rbTrueModif
            // 
            this.rbTrueModif.AutoSize = true;
            this.rbTrueModif.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rbTrueModif.Enabled = false;
            this.rbTrueModif.FlatAppearance.BorderSize = 0;
            this.rbTrueModif.ForeColor = System.Drawing.Color.Snow;
            this.rbTrueModif.Location = new System.Drawing.Point(399, 343);
            this.rbTrueModif.Name = "rbTrueModif";
            this.rbTrueModif.Size = new System.Drawing.Size(41, 17);
            this.rbTrueModif.TabIndex = 31;
            this.rbTrueModif.Text = "Oui";
            this.rbTrueModif.UseVisualStyleBackColor = true;
            // 
            // tbModifMontant
            // 
            this.tbModifMontant.Location = new System.Drawing.Point(352, 295);
            this.tbModifMontant.Name = "tbModifMontant";
            this.tbModifMontant.Size = new System.Drawing.Size(281, 20);
            this.tbModifMontant.TabIndex = 30;
            // 
            // tbModifJustificatif
            // 
            this.tbModifJustificatif.Location = new System.Drawing.Point(352, 232);
            this.tbModifJustificatif.Margin = new System.Windows.Forms.Padding(0);
            this.tbModifJustificatif.Name = "tbModifJustificatif";
            this.tbModifJustificatif.Size = new System.Drawing.Size(281, 20);
            this.tbModifJustificatif.TabIndex = 29;
            // 
            // tbModifDesignation
            // 
            this.tbModifDesignation.Location = new System.Drawing.Point(352, 187);
            this.tbModifDesignation.Name = "tbModifDesignation";
            this.tbModifDesignation.Size = new System.Drawing.Size(281, 20);
            this.tbModifDesignation.TabIndex = 28;
            // 
            // labelModifReparti
            // 
            this.labelModifReparti.AutoSize = true;
            this.labelModifReparti.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.labelModifReparti.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelModifReparti.ForeColor = System.Drawing.Color.Snow;
            this.labelModifReparti.Location = new System.Drawing.Point(196, 344);
            this.labelModifReparti.Name = "labelModifReparti";
            this.labelModifReparti.Size = new System.Drawing.Size(58, 16);
            this.labelModifReparti.TabIndex = 27;
            this.labelModifReparti.Text = "Reparti :";
            // 
            // labelModifJustificatif
            // 
            this.labelModifJustificatif.AutoSize = true;
            this.labelModifJustificatif.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.labelModifJustificatif.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelModifJustificatif.ForeColor = System.Drawing.Color.Snow;
            this.labelModifJustificatif.Location = new System.Drawing.Point(193, 252);
            this.labelModifJustificatif.Name = "labelModifJustificatif";
            this.labelModifJustificatif.Size = new System.Drawing.Size(71, 16);
            this.labelModifJustificatif.TabIndex = 26;
            this.labelModifJustificatif.Text = "Justificatif :";
            // 
            // labelModifDesignation
            // 
            this.labelModifDesignation.AutoSize = true;
            this.labelModifDesignation.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.labelModifDesignation.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelModifDesignation.ForeColor = System.Drawing.Color.Snow;
            this.labelModifDesignation.Location = new System.Drawing.Point(193, 191);
            this.labelModifDesignation.Name = "labelModifDesignation";
            this.labelModifDesignation.Size = new System.Drawing.Size(86, 16);
            this.labelModifDesignation.TabIndex = 25;
            this.labelModifDesignation.Text = "Désignation :";
            // 
            // labelModifMontant
            // 
            this.labelModifMontant.AutoSize = true;
            this.labelModifMontant.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.labelModifMontant.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelModifMontant.ForeColor = System.Drawing.Color.Snow;
            this.labelModifMontant.Location = new System.Drawing.Point(193, 299);
            this.labelModifMontant.Name = "labelModifMontant";
            this.labelModifMontant.Size = new System.Drawing.Size(61, 16);
            this.labelModifMontant.TabIndex = 24;
            this.labelModifMontant.Text = "Montant :";
            // 
            // labelModifDateDepense
            // 
            this.labelModifDateDepense.AutoSize = true;
            this.labelModifDateDepense.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.labelModifDateDepense.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelModifDateDepense.ForeColor = System.Drawing.Color.Snow;
            this.labelModifDateDepense.Location = new System.Drawing.Point(193, 143);
            this.labelModifDateDepense.Name = "labelModifDateDepense";
            this.labelModifDateDepense.Size = new System.Drawing.Size(133, 16);
            this.labelModifDateDepense.TabIndex = 23;
            this.labelModifDateDepense.Text = "Date de la dépense :";
            // 
            // dtpModifDate
            // 
            this.dtpModifDate.Location = new System.Drawing.Point(352, 143);
            this.dtpModifDate.Name = "dtpModifDate";
            this.dtpModifDate.Size = new System.Drawing.Size(281, 20);
            this.dtpModifDate.TabIndex = 22;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Snow;
            this.label1.Location = new System.Drawing.Point(150, 105);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(176, 16);
            this.label1.TabIndex = 35;
            this.label1.Text = "Sélectionner un colocataire :";
            // 
            // cbColocataire
            // 
            this.cbColocataire.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbColocataire.FormattingEnabled = true;
            this.cbColocataire.Location = new System.Drawing.Point(352, 105);
            this.cbColocataire.Name = "cbColocataire";
            this.cbColocataire.Size = new System.Drawing.Size(281, 21);
            this.cbColocataire.TabIndex = 34;
            // 
            // btnRetourDepense
            // 
            this.btnRetourDepense.BackColor = System.Drawing.Color.Snow;
            this.btnRetourDepense.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRetourDepense.FlatAppearance.BorderSize = 0;
            this.btnRetourDepense.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRetourDepense.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRetourDepense.Location = new System.Drawing.Point(12, 378);
            this.btnRetourDepense.Name = "btnRetourDepense";
            this.btnRetourDepense.Size = new System.Drawing.Size(117, 60);
            this.btnRetourDepense.TabIndex = 37;
            this.btnRetourDepense.Text = "Retourner sur la page précédente ↩️";
            this.btnRetourDepense.UseVisualStyleBackColor = false;
            // 
            // od_ouvrir
            // 
            this.od_ouvrir.FileName = "openFileDialog1";
            // 
            // btnOpen
            // 
            this.btnOpen.BackColor = System.Drawing.Color.Snow;
            this.btnOpen.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnOpen.FlatAppearance.BorderSize = 0;
            this.btnOpen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOpen.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.btnOpen.Location = new System.Drawing.Point(352, 252);
            this.btnOpen.Margin = new System.Windows.Forms.Padding(0);
            this.btnOpen.Name = "btnOpen";
            this.btnOpen.Size = new System.Drawing.Size(281, 23);
            this.btnOpen.TabIndex = 38;
            this.btnOpen.Text = "Sélectionner un fichier 📁";
            this.btnOpen.UseVisualStyleBackColor = false;
            // 
            // FModificationDepense
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.ClientSize = new System.Drawing.Size(889, 450);
            this.Controls.Add(this.btnOpen);
            this.Controls.Add(this.btnRetourDepense);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbColocataire);
            this.Controls.Add(this.btnModificationDepense);
            this.Controls.Add(this.rbFalseModif);
            this.Controls.Add(this.rbTrueModif);
            this.Controls.Add(this.tbModifMontant);
            this.Controls.Add(this.tbModifJustificatif);
            this.Controls.Add(this.tbModifDesignation);
            this.Controls.Add(this.labelModifReparti);
            this.Controls.Add(this.labelModifJustificatif);
            this.Controls.Add(this.labelModifDesignation);
            this.Controls.Add(this.labelModifMontant);
            this.Controls.Add(this.labelModifDateDepense);
            this.Controls.Add(this.dtpModifDate);
            this.Controls.Add(this.labelModifDepense);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FModificationDepense";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Modifier une dépense";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelModifDepense;
        private System.Windows.Forms.Button btnModificationDepense;
        private System.Windows.Forms.RadioButton rbFalseModif;
        private System.Windows.Forms.RadioButton rbTrueModif;
        private System.Windows.Forms.TextBox tbModifMontant;
        private System.Windows.Forms.TextBox tbModifJustificatif;
        private System.Windows.Forms.TextBox tbModifDesignation;
        private System.Windows.Forms.Label labelModifReparti;
        private System.Windows.Forms.Label labelModifJustificatif;
        private System.Windows.Forms.Label labelModifDesignation;
        private System.Windows.Forms.Label labelModifMontant;
        private System.Windows.Forms.Label labelModifDateDepense;
        private System.Windows.Forms.DateTimePicker dtpModifDate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbColocataire;
        private System.Windows.Forms.Button btnRetourDepense;
        private System.Windows.Forms.OpenFileDialog od_ouvrir;
        private System.Windows.Forms.Button btnOpen;
    }
}