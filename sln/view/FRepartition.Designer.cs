﻿
namespace Coloc.View
{
    partial class FRepartition
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FRepartition));
            this.btnRetourRepartition = new System.Windows.Forms.Button();
            this.lbRepartation = new System.Windows.Forms.ListBox();
            this.labelConsultationColocs = new System.Windows.Forms.Label();
            this.btnSolderPeriode = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnRetourRepartition
            // 
            this.btnRetourRepartition.BackColor = System.Drawing.Color.Snow;
            this.btnRetourRepartition.FlatAppearance.BorderSize = 0;
            this.btnRetourRepartition.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRetourRepartition.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRetourRepartition.Location = new System.Drawing.Point(52, 277);
            this.btnRetourRepartition.Name = "btnRetourRepartition";
            this.btnRetourRepartition.Size = new System.Drawing.Size(117, 60);
            this.btnRetourRepartition.TabIndex = 16;
            this.btnRetourRepartition.Text = "Retourner sur la page précédente ↩️";
            this.btnRetourRepartition.UseVisualStyleBackColor = false;
            // 
            // lbRepartation
            // 
            this.lbRepartation.FormattingEnabled = true;
            this.lbRepartation.Location = new System.Drawing.Point(218, 93);
            this.lbRepartation.Name = "lbRepartation";
            this.lbRepartation.Size = new System.Drawing.Size(521, 290);
            this.lbRepartation.TabIndex = 15;
            // 
            // labelConsultationColocs
            // 
            this.labelConsultationColocs.AutoSize = true;
            this.labelConsultationColocs.BackColor = System.Drawing.Color.Transparent;
            this.labelConsultationColocs.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelConsultationColocs.ForeColor = System.Drawing.Color.Snow;
            this.labelConsultationColocs.Location = new System.Drawing.Point(310, 44);
            this.labelConsultationColocs.Name = "labelConsultationColocs";
            this.labelConsultationColocs.Size = new System.Drawing.Size(347, 29);
            this.labelConsultationColocs.TabIndex = 14;
            this.labelConsultationColocs.Text = "Consultation des répartitions";
            // 
            // btnSolderPeriode
            // 
            this.btnSolderPeriode.BackColor = System.Drawing.Color.Snow;
            this.btnSolderPeriode.FlatAppearance.BorderSize = 0;
            this.btnSolderPeriode.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSolderPeriode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSolderPeriode.Location = new System.Drawing.Point(52, 142);
            this.btnSolderPeriode.Name = "btnSolderPeriode";
            this.btnSolderPeriode.Size = new System.Drawing.Size(117, 50);
            this.btnSolderPeriode.TabIndex = 17;
            this.btnSolderPeriode.Text = "Solder une période 💰";
            this.btnSolderPeriode.UseVisualStyleBackColor = false;
            // 
            // FRepartition
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.ClientSize = new System.Drawing.Size(785, 414);
            this.Controls.Add(this.btnSolderPeriode);
            this.Controls.Add(this.btnRetourRepartition);
            this.Controls.Add(this.lbRepartation);
            this.Controls.Add(this.labelConsultationColocs);
            this.ForeColor = System.Drawing.Color.Black;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FRepartition";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Répartir les dépenses";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnRetourRepartition;
        private System.Windows.Forms.ListBox lbRepartation;
        private System.Windows.Forms.Label labelConsultationColocs;
        private System.Windows.Forms.Button btnSolderPeriode;
    }
}