﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Coloc.Dao;
using Coloc.Model;

namespace Coloc.View
{
    public partial class FRepartition : Form
    {
        public FRepartition()
        {
            InitializeComponent();
            btnRetourRepartition.Click += BtnRetourRepartition_Click;
            DaoColoc daoColoc = new DaoColoc();
            this.LoadListe(daoColoc.GetAll());
            this.FormClosing += FRepartition_FormClosing;
            this.btnSolderPeriode.Click += BtnSolderPeriode_Click;
        }

        private void BtnSolderPeriode_Click(object sender, EventArgs e)
        {
            var result = MessageBox.Show("Veux-tu vraiment solder la période ?", "⚠ Warning ⚠️",
                                 MessageBoxButtons.YesNo,
                                 MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                DaoDepense daoDepense = new DaoDepense();
                List<Depense> lesDepenses;
                lesDepenses = daoDepense.GetAllNonReparti();
                foreach (Depense uneDepense in lesDepenses)
                {
                    uneDepense.Reparti = true;
                    daoDepense.Update(uneDepense);
                }
                lbRepartation.Items.Clear();
                DaoColoc daoColoc = new DaoColoc();
                this.LoadListe(daoColoc.GetAll());
            }
            else
            {
                return;
            } 
        }

        private void FRepartition_FormClosing(object sender, FormClosingEventArgs e)
        {
            FMenu f = new FMenu();
            f.Show();
        }

        private void BtnRetourRepartition_Click(object sender, EventArgs e)
        {
            this.Hide();
            FMenu f = new FMenu();
            f.Show();

        }

        public void LoadListe(List<Colocataire> listeColoc)
        {

            foreach (Colocataire c in listeColoc)
            {
                DaoColoc daoColoc = new DaoColoc();
                DepensesNonReparties depenseNonreparti = new DepensesNonReparties();
                DaoDepense daoDepense = new DaoDepense();
                c.SetListDepense(daoDepense.GetAllDepense(c.IdColoc));

                decimal montantDu = depenseNonreparti.MontantDu(daoColoc.GetCout(),daoDepense.GetAllNonReparti());
                lbRepartation.Items.Add(String.Format("Qui : {0}     a Payé : {1:0.00}     aurait du payer : {2:0.00}     solde à régler : {3:0.00}", c.Nom,c.APaye(),montantDu,c.Doit(montantDu)));
            }
        }
    }
}
