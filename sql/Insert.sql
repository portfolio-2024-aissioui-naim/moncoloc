insert into colocataire(prenom,nom, mail, telephone) values("Bob","Wazowski","bob@gmail.com","06 06 06 06 06");
insert into colocataire(prenom,nom, mail, telephone) values("Alice","AuPaysDesMerveilles","aliceapdm@gmail.com","06 06 06 06 07");
insert into colocataire(prenom,nom, mail, telephone) values("Zack","Effron","zacklebg@gmail.com","06 06 06 06 08");




insert into depense(dateDepense, text, justificatif, montant, reparti, idColocataire) values("2023-05-01", "Achat épicerie leclerc", "ticket.jpg", 75.50, FALSE, 1);
insert into depense(dateDepense, text, justificatif, montant, reparti, idColocataire) values("2023-05-12", "Paiement facture électricité", "facEDF.pdf", 35.75, FALSE, 2);
insert into depense(dateDepense, text, justificatif, montant, reparti, idColocataire) values("2023-05-25", "Paiement facture eau", "facEau.pdf", 120.85, FALSE, 3);
insert into depense(dateDepense, text, justificatif, montant, reparti, idColocataire) values("2023-05-17", "Achat recharge encre imprimante", "ticketauchan.jpg", 30.00, FALSE, 3);